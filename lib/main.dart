import 'package:flutter/material.dart';
import 'package:nim_simple_get_api/get_model.dart';
import 'package:nim_simple_get_api/post_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  PostModel postModel = null;
  User user = null;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // appBar: AppBar(
        //   title: Text("API"),
        // ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Padding(padding: EdgeInsets.fromLTRB(30, 0, 0, 0)),
              Text(
                "WELCOME!",
                style: TextStyle(fontSize: 48, color: Color(0xFF0563BA)),
              ),
              Image.network(
                ((user != null) ? user.avatar : "Tidak ada data"),
                width: 128,
                height: 128,
              ),
              ListTile(
                title: Text(
                  (user != null) ? user.name : "Tidak ada data",
                  style: TextStyle(
                    fontSize: 24,
                    color: Color(0xFF0563BA),
                  ),
                ),
                subtitle: Text(
                  (user != null) ? user.email : "Tidak ada data",
                  style: TextStyle(
                    fontSize: 24,
                    color: Color(0xFF0563BA),
                  ),
                ),
              ),
              RaisedButton(
                onPressed: () {
                  User.connectToAPI("2").then(
                    (value) {
                      user = value;
                      setState(() {});
                    },
                  );
                },
                child: Text("GET"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
