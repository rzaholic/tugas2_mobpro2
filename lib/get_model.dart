import 'dart:convert';

import 'package:http/http.dart' as http;

class User {
  String id, name, avatar, email;

  User({this.id, this.name, this.avatar, this.email});

  factory User.createUser(Map<String, dynamic> object) {
    return User(
        id: object['id'].toString(),
        name: object['first_name'] + " " + object['last_name'],
        avatar: object['avatar'],
        email: object ['email']);
  }

  static Future<User> connectToAPI(String id) async {
    String apiURL = "https://reqres.in/api/users/" + id;

    var apiResult = await http.get(apiURL);

    var jsonObject = json.decode(apiResult.body);

    var userData =
        (jsonObject as Map<String, dynamic>)['data']; // mengeluarkan data json

    return User.createUser(userData);
  }
}
