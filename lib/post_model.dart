import 'dart:convert';

import 'package:http/http.dart' as http;

class PostModel {
  String id, name, job, created;

  // constructor
  PostModel({this.id, this.name, this.job, this.created});

  // factory method
  factory PostModel.createPostModel(Map<String, dynamic> object) {
    return PostModel(
        id: object['id'],
        name: object['name'],
        job: object['job'],
        created: object['createdAt']);
  }

  // method untuk menghubungkan ke API
  static Future<PostModel> connectToAPI(String name, String job) async {
    String apiURL = "https://reqres.in/api/users";

    var apiResult = await http.post(apiURL, body: {"name": name, "job": job});

    // ambil bentuk JSON
    var jsonObject = json.decode(apiResult.body);

    return PostModel.createPostModel(jsonObject);
  }
}
